const courseMiddleWare =  (req, res, next) => {
    console.log("Course Middleware123 - Time: " + new Date() + " - Method: " + req.method);

    next();
}

const getAllCoursesMiddleWare =  (req, res, next) => {
    console.log("Get All Course Middleware");

    next();
}

const getCourseByIdMiddleWare =  (req, res, next) => {
    console.log("Get A Course By Id Middleware");

    next();
}

const postCoursesMiddleWare =  (req, res, next) => {
    console.log("Create new Course Middleware");

    next();
}

const putCourseByIdMiddleWare =  (req, res, next) => {
    console.log("Update a Course By Id Middleware");

    next();
}

const deleteCourseByIdMiddleWare =  (req, res, next) => {
    console.log("Delete a Course By Id Middleware");

    next();
}

module.exports = {
    courseMiddleWare,
    getAllCoursesMiddleWare,
    getCourseByIdMiddleWare,
    postCoursesMiddleWare,
    putCourseByIdMiddleWare,
    deleteCourseByIdMiddleWare
}
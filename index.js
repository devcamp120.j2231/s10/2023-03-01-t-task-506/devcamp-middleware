//Khai báo thư viện express
const express = require("express");

//Khai báo thư viện mongoose
const mongoose = require("mongoose");

const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

// Gọi các model (Tạm thời)
const reviewModel = require("./app/models/reviewModel"); 
const courseModel = require("./app/models/courseModel"); 

//Khởi tạo app express
const app = express();

//Khai báo cổng
const port = 8000;

// Kết nối với CSDL (Sử dụng mongoose theo phiên bản 6.x)
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course", function(error) {
    if (error) throw error;
    console.log('Successfully connected');
})

//Khai báo sử dụng json
app.use(express.json());

/**** Task 506.10 Application Middleware ******/
const methodMiddleWare =  (req, res, next) => {
    console.log(req.method);

    next();
}



app.use((req, res, next) => {
    console.log(new Date());

    next();
}, methodMiddleWare)

app.use('/get-method', (req, res, next) => {
    console.log('Middleware for all method of an URL');

    next();
})

app.get('/get-method', (req, res, next) => {
    console.log('Middleware for only Get method');

    next();
})

/*
app.use((req, res, next) => {
    console.log(req.method);

    next();
})*/

app.get('/get-method', (req, res) => {
    console.log('Get-method demo');
    res.json({
        message:'Get-method'
    })
})

app.post('/get-method', (req, res) => {
    console.log('Post-method demo');
    res.json({
        message:'Post-method'
    })
})

app.get('/', (req, res) => {
    var date = new Date();
    console.log('middelware demo');
    res.json({
        message:`Hôm nay là ngày ${date.getDate()} tháng ${date.getMonth()+1} năm ${date.getFullYear()}`
    })
})

/**** Task 506.20 Router Middleware ******/
app.use('/', courseRouter);
app.use('/', reviewRouter);

//Start app tại port 8000
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})